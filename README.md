# Terraform Provider TPLink Print Server

Run the following command to build the provider

```shell
go build .
```

## Do release

Binaries for all Go supported platforms can be done with:

```shell
make release
```

## Development Notes

The terraform [sdk repo] is a good starting point for docs.

Eventually it will be nice to have docs for users of this provider.
There's [a tool] for generating those docs so need to look into that.

Need to figure out how to communicate with the print server.  One option
is to use the http server but another might be to use [telnet].

[sdk repo]: https://github.com/hashicorp/terraform-plugin-sdk
[a tool]: https://github.com/hashicorp/terraform-plugin-docs
[telnet]: https://pkg.go.dev/github.com/reiver/go-telnet#hdr-Example_TELNET_Client
