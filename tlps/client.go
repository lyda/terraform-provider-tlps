//
// client.go
// Copyright (C) 2021 Kevin Lyda <kevin@lyda.ie>
//
// Distributed under terms of the GPL license.
//

package tlps

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"path"
	"time"
)

type Client struct {
	Client   *http.Client
	URL      string
	Account  string
	Password string
}

func NewClient(url string, account string, password string) (*Client, error) {
	c := Client{
		Client:   &http.Client{Timeout: 10 * time.Second},
		URL:      url,
		Account:  account,
		Password: password,
	}
	return &c, nil
}

func (c *Client) call(method string, trailing string) (string, error) {
	req, err := http.NewRequest(method, path.Join(c.URL, trailing), nil)
	if err != nil {
		panic(err.Error())
	}
	req.SetBasicAuth(c.Account, c.Password)
	response, err := c.Client.Do(req)
	if err != nil {
		panic(err.Error())
	}
	dump, err := httputil.DumpResponse(response, true)
	fmt.Printf("%q", dump)
	defer response.Body.Close()
	return "", nil
}
