module gitlab.com/lyda/terraform-provider-tlps

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.7.0 // indirect
	github.com/ValeLint/gospell v0.0.0-20190917205818-a0d53c86ac15 // indirect
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/errata-ai/vale v1.7.1 // indirect
	github.com/fatih/color v1.12.0 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/golang/snappy v0.0.3 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/terraform-plugin-sdk/v2 v2.7.0
	github.com/jdkato/prose v1.2.1 // indirect
	github.com/jdkato/regexp v0.1.0 // indirect
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/levigross/grequests v0.0.0-20190908174114-253788527a1a // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/mmcdole/gofeed v1.1.3 // indirect
	github.com/mmcdole/goxpp v0.0.0-20200921145534-2f3784f67354 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/montanaflynn/stats v0.6.6 // indirect
	github.com/nwaples/rardecode v1.1.0 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/pierrec/lz4 v2.6.1+incompatible // indirect
	github.com/remeh/sizedwaitgroup v1.0.0 // indirect
	github.com/russross/blackfriday v1.6.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/shogo82148/go-shuffle v1.0.0 // indirect
	github.com/spf13/afero v1.6.0 // indirect
	github.com/ulikunitz/xz v0.5.10 // indirect
	github.com/urfave/cli v1.22.5 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
	golang.org/x/tools v0.1.4 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
	gopkg.in/neurosnap/sentences.v1 v1.0.7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
